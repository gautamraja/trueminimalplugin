namespace Photon.Hive.Plugin.TrueMinimal
{
    internal class Actor
    {
        #region Public Properties

        public int ActorNr { get; set; }

        public string UserId { get; set; }

        #endregion
    }
}