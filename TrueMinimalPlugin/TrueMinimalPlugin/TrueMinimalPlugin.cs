﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Photon.Hive.Plugin.TrueMinimal
{
    public class TrueMinimalPlugin : PluginBase
    {

        #region Constants
        #endregion

        #region Fields

        #endregion

        #region Public Properties

        //public override bool IsPersistent
        //{
        //    get
        //    {
        //        return this.isPersistentFlag && !string.IsNullOrEmpty(this.gameCreatedUrl) && !string.IsNullOrEmpty(this.gameClosedUrl);
        //    }
        //}

        public override string Name
        {
            get
            {
                return "TrueMinimalPlugin";
            }
        }

        #endregion

        public TrueMinimalPlugin()
        {
            this.UseStrictMode = true;
        }

        #region Public Methods and Operators

        public void RaiseEvent(byte eventCode, object eventData,
            byte receiverGroup = ReciverGroup.All,
            int senderActorNumber = 0,
            byte cachingOption = CacheOperations.DoNotCache,
            byte interestGroup = 0,
            SendParameters sendParams = default(SendParameters))
        {
            Dictionary<byte, object> parameters = new Dictionary<byte, object>();
            parameters.Add(245, eventData);
            parameters.Add(254, senderActorNumber);
            PluginHost.BroadcastEvent(receiverGroup, senderActorNumber, interestGroup, eventCode, parameters, cachingOption, sendParams);
        }

        public override void OnRaiseEvent(IRaiseEventCallInfo info)
        {
            base.OnRaiseEvent(info);

            byte eventCode = info.Request.EvCode;

            switch (eventCode)
            {
                case (byte)0:

                    BroadcastEvent(99, new Dictionary<byte, object>()
                        {
                            {
                            245, "Hello World from Server!"
                            }
                        });
                    break;
            }
        }

        #endregion

        #region Methods

        #endregion
    }
}
